var Mongoose   = require('mongoose');
var Schema     = Mongoose.Schema;

// The data schema for an event that we're tracking in our analytics engine
var Students = new Schema({
  name      : { type: String, required: true, trim: true },
  stno        : { type: String, required: true, trim: true }
});

var StudentModel = Mongoose.model('students', Students);

module.exports = {
  StudentModel: StudentModel
};
