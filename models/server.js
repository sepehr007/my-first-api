
var Hapi 		= require('hapi');
var Mongoose    = require('mongoose');

var stModel = require("./models/student").StudentModel;

var server 	    = new Hapi.Server();
server.connection({
	host: "127.0.0.1",
	port: 4545
});
// MongoDB Connection
Mongoose.connect('mongodb://localhost:27017/Sepehr');


function getMethod(req, res){
	stModel.find().exec(function(err, havij){
		if(err){
			console.log(err);
				return res({
					msg: err,
					code: 500
				}).code(500);
		}
		else{
			return res({
					msg: "fetched to database",
					data: havij,
					code: 200
				}).code(200);
		}
	});

	};

	function postMethod (req, res){

		var newStudent = new stModel({
			name: req.payload.name,
			stno : req.payload.stno
		});

		newStudent.save(function(err){
			if(err){
				console.log(err);
				return res({
					msg: err,
					code: 500
				}).code(500);
			}
			else{
				return res({
					msg: "Added to database",
					data: req.payload.name + " : " + req.payload.stno,
					code: 200
				}).code(200);
			}
		});
		
	};


function getStudentByStno(req, res){
	var stnoo = req.params.stno;
	stModel.findOne({'stno' : stnoo}).exec(function(err, havij){
		if(err){
			console.log(err);
				return res({
					msg: err,
					code: 500
				}).code(500);
		}
		else if(!havij){
				return res({
					msg: "not found",
					code: 404
				}).code(404);
		}
		else{
			return res({
					msg: "fetched to database",
					data: havij,
					code: 200
				}).code(200);
		}
	});
}

function deleteStudentByStno(req, res){
	var stno = req.params.stno;
	stModel.remove({'stno' : stno}).exec(function(err, havij){
		if(err){
			console.log(err);
				return res({
					msg: err,
					code: 500
				}).code(500);
		}
		else if(!havij){
				return res({
					msg: "not found",
					code: 404
				}).code(404);
		}
		else{
			return res({
					msg: "fetched to database",
					data: havij,
					code: 200
				}).code(200);
		}
	});
}

function updateStudentBystno(req, res){
	var stno=req.params.stno;

	var changes={$set:{
			'name':req.payload.name,
			'stno':req.payload.stno
		}};
		stModel.update({'stno':stno},changes,{multi :true},function(err,zahr){
			if(!err)
			{
					return res({
					msg: "fetched to database",
					data: zahr,
					code: 200
				}).code(200);
			}
		});

// the other way :)))))

/*	var changes={$set:{
		'name':req.payload.name,
		'stno':req.payload.stno
	}};
	stModel.findOneAndUpdate({'stno':stno},changes, {new: true},function(err,maraz){
		if(!err)
		{
				return res({
				msg: "fetched to database",
				data: maraz,
				code: 200
			}).code(200);
		}
		
	});*/
};

server.route({
	method: 'GET',
	path: '/{stno}',
	handler: getStudentByStno
});

server.route({
	method: 'DELETE',
	path: '/{stno}',
	handler: deleteStudentByStno
});


server.route({
	method: 'GET',
	path: '/',
	handler: getMethod
});


server.route({
	method: 'POST',
	path: '/',
	handler: postMethod
});

server.route({
	method: 'PUT',
	path: '/{stno}',
	handler: updateStudentBystno
});

server.start(function () {
    console.log('Server started at: ' + server.info.uri);
});
